# README
 You will need Python 3 to run this.
 Minesweepercl makes use of the Python curses module,
 so it will only run on a Unix like terminal.
 (It might work in Cygwin, but I don't know...)
 Assuming you are on Mac or Linux, and have installed
 Python 3, navigate to the file in Terminal, and type
 
     ./minesweeper.py
     
 If it doesn't work, try typing
 
     python3 ./minesweeper.py
     
 If that works, then the file is probably not executable.
 You can check this by typing
 
     ls -l
     
 If minesweeper.py has the permissions `rw_rw_rw`
 (or something like that...), then type into Terminal
 
     chmod +x minesweeper.py
     
 This will make it executable.